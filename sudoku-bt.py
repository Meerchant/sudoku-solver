



def findNextCellToFill(grid, i, j):
    for x in range(i, 9):
        for y in range(j, 9):
            if grid[x][y] == 0:
                return x, y
    for x in range(0, 9):
        for y in range(0, 9):
            if grid[x][y] == 0:
                return x, y
    return -1, -1


def spr(wektor):

    spr = []
    x=1
    while x < 10:

        if x in wektor:
            spr.extend([x])


        if wektor[x-1]==0:
            spr.extend([0])
        x = x + 1

    wektor.sort()
    spr.sort()

    if wektor==spr:
        return True
    else: return False

def spr_wiersze(tab):
    i=0
    while i<=8:
       if spr(tab[i][:]):
           i=i+1
       else:
           return False

    return True

def spr_kolumny(tab):
    i=0
    ws=[0,0,0,0,0,0,0,0,0]
    while i<9:
        k=0
        while k<9:
            ws[k]=tab[k][i]
            k=k+1

        if spr(ws):
            i=i+1

        else:
           return False

    return  True


def spr_kwadrat(tab,a,b):

    ws=[0,0,0,0,0,0,0,0,0]

    ws[0] = tab[a][b]
    ws[1] = tab[a][b+1]
    ws[2] = tab[a][b+2]
    ws[3] = tab[a+1][b]
    ws[4] = tab[a+1][b+1]
    ws[5] = tab[a+1][b+2]
    ws[6] = tab[a+2][b]
    ws[7] = tab[a+2][b+1]
    ws[8] = tab[a+2][b+2]

    if spr(ws):

        return True

    else:

        return False

def spr_kwadraty(tab):

    if spr_kwadrat(tab, 0, 0)&spr_kwadrat(tab, 0, 3)&spr_kwadrat(tab, 0, 6)&spr_kwadrat(tab, 3, 0)&spr_kwadrat(tab, 3, 3)&spr_kwadrat(tab, 3, 6)&spr_kwadrat(tab, 6, 0)&spr_kwadrat(tab, 6, 3)&spr_kwadrat(tab, 6, 6):
        return True
    else:
        return False

def ktory_kwadrat(a,b,tab):
    x=0
    y=0

    ws=[0,0,0,0,0,0,0,0,0]
    if a<3:
        x=0
    if a<6&a>2:
        x=3
    if a<9&a>5:
        x=6
    if b<3:
        y=0
    if b<6&b>2:
        y=3
    if b<9&b>5:
        y=6
    ws[0] = tab[x][y]
    ws[1] = tab[x][y + 1]
    ws[2] = tab[x][y + 2]
    ws[3] = tab[x + 1][y]
    ws[4] = tab[x + 1][y + 1]
    ws[5] = tab[x + 1][y + 2]
    ws[6] = tab[x + 2][y]
    ws[7] = tab[x + 2][y + 1]
    ws[8] = tab[x + 2][y + 2]
    return ws






def isGood(tab):


    if spr_wiersze(tab) & spr_kolumny(tab) & spr_kwadraty(tab):
        return True

    return False



licznik=0



def solveSudoku(grid, i=0, j=0):
    global licznik

    i, j = findNextCellToFill(grid, i, j)
    if i == -1:
        return True


    possible = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    dl=9

    for x in range(9):

        if grid[i][x - 1] != 0:
            y = grid[i][x - 1]
            dl=dl-1
            possible.remove(y)

        if grid[x - 1][j] != 0 & grid[x - 1][j] in possible:
            k = grid[x - 1][j]
            dl=dl-1
            possible.remove(k)

        qwe = ktory_kwadrat(i, j, grid)
        if qwe[x - 1] != 0 & qwe[x - 1] in possible:
            g = qwe[x - 1]
            dl=dl-1
            possible.remove(g)


    for e in range(dl):

        grid[i][j]=possible[e]
        if isGood(grid):
            grid[i][j] = possible[e]
            for f in range(9):
                print(tablica[f][:])
            print("                 ")
            licznik=licznik+1
            print(licznik)
            if solveSudoku(grid, i, j):
                return True
            # Undo the current cell for backtracking
            grid[i][j] = 0
        else:
            grid[i][j]=0
    return False



tablica = [[0, 1, 0, 0, 3, 4, 0, 0, 0],
           [0, 0, 0, 0, 7, 0, 0, 2, 0],
           [3, 4, 2, 0, 0, 0, 0, 0, 0],
           [4, 3, 8, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 5, 6, 0],
           [0, 0, 0, 0, 4, 9, 0, 0, 0],
           [8, 0, 0, 7, 0, 0, 0, 0, 9],
           [0, 0, 0, 9, 0, 5, 1, 0, 7],
           [9, 0, 3, 0, 0, 0, 0, 0, 0]]




solveSudoku(tablica)
for f in range(9):
    print(tablica[f][:])